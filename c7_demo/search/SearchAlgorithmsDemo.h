#pragma once
#include "c7/data/Data.h"
#include "c7/utils/Timer.h"
#include "c7_demo/search/algorithms/AbstractSearchDemo.h"

using namespace c7;


// Separate namespace created to avoid situations like: "using c7" (which will get also demo classes)
namespace c7_demo
{
	/**
	 * Show all of Search Algorithms demo
	 */
	class SearchAlgorithmsDemo
	{
	public:
		SearchAlgorithmsDemo();
		~SearchAlgorithmsDemo();

		void run();

	private:
		Data *data;
		Timer *timer;

		// demo
		AbstractSearchDemo *binarySearchDemo;
	};
}
