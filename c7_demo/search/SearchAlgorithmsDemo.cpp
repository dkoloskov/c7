#include "SearchAlgorithmsDemo.h"
#include "c7_demo/search/algorithms/BinarySearchDemo.h"

using namespace c7;
using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
SearchAlgorithmsDemo::SearchAlgorithmsDemo()
{
	data = new Data();
	timer = new Timer();

	// demo's
	binarySearchDemo = new BinarySearchDemo(data, timer);
}

void SearchAlgorithmsDemo::run()
{
	//
	binarySearchDemo->run();

	//
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
SearchAlgorithmsDemo::~SearchAlgorithmsDemo()
{
	delete timer;
	delete data;

	delete binarySearchDemo;
}
