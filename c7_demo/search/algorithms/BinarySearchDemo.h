#pragma once
#include "AbstractSearchDemo.h"


namespace c7_demo
{
	class BinarySearchDemo : public AbstractSearchDemo
	{
	public:
		BinarySearchDemo(const Data *data, Timer *timer);

		virtual void run() override;
	};
}
