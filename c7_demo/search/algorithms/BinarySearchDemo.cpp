#include "BinarySearchDemo.h"
#include "c7/search/algorithms/BinarySearch.h"

using namespace c7_demo;

//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
BinarySearchDemo::BinarySearchDemo(const Data *data, Timer *timer):
	AbstractSearchDemo(data, timer)
{
	// Algorithm
	algorithmName = "Binary Search";
	searchAlgorithm = new BinarySearch();

	// Input data
	inputArray = data->sorted.primeNumbers;
	searchedValue = 11;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
void BinarySearchDemo::run()
{
	AbstractSearchDemo::run();

	// ======================================== Run with: [100 / 1k / 1m] input data
	// 100
	/*inputArray = data->sorted.hundredNumbers;
	searchedValue = 48;
	AbstractSearchDemo::run();*/

	// 1k
	/*inputArray = data->sorted.thousandNumbers;
	searchedValue = 490;
	AbstractSearchDemo::run();*/

	/*inputArray = data->sorted.millionNumbers;
	searchedValue = 777888;
	AbstractSearchDemo::run();*/
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
