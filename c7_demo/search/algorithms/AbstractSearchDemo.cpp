#include "AbstractSearchDemo.h"
#include "c7/utils/Log.h"
#include <string>

using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
void AbstractSearchDemo::run()
{
	inputLog();
	findMaxRounds();
	findValue();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
AbstractSearchDemo::AbstractSearchDemo(const Data *data, Timer *timer)
{
	this->data = data;
	this->timer = timer;
}

// log (all the usefull info)
void AbstractSearchDemo::inputLog()
{
	// ======================================== LOG (all the usefull info)
	Log::title(algorithmName);
	Log::out("Input:");
	if (inputArray.size() <= 100) // Don't want many numbers in log
		Log::out(inputArray);

	Log::out("	array size: ", inputArray.size());
	Log::out("	searched value: ", searchedValue);
	Log::line();
}

void AbstractSearchDemo::findMaxRounds()
{
	int maxRounds = searchAlgorithm->findMaxRounds(inputArray.size());
	Log::out("Max rounds to find value: ", maxRounds);
	Log::line();
}

void AbstractSearchDemo::findValue()
{
	// ======================================== Search
	timer->reset();
	SearchResult result = searchAlgorithm->find(inputArray, searchedValue);
	double elapsedTime = timer->elapsed();

	// ======================================== LOG (all the usefull info) - part 2
	Log::line();
	Log::out("Result:");
	Log::out("	searched value index: ", result.foundIndex);
	Log::out("	rounds to find value: ", result.rounds);
	Log::out("	search time: " + to_string(elapsedTime) + " seconds");
	Log::blank();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
AbstractSearchDemo::~AbstractSearchDemo()
{
	data = nullptr;
	timer = nullptr;

	//
	delete searchAlgorithm;
	searchAlgorithm = nullptr;
}
