#pragma once
#include "c7/data/Data.h"
#include "c7/utils/Timer.h"
#include "c7/search/algorithms/AbstractSearch.h"
#include <string>

using namespace c7;


/**
 * Base class for all Search Algorithm Demo classes
 */
namespace c7_demo
{
	class AbstractSearchDemo
	{
	public:
		virtual ~AbstractSearchDemo();

		virtual void run();

	protected:
		AbstractSearchDemo(const Data *data, Timer *timer);

		const Data *data;
		Timer *timer;

		std::string algorithmName;
		AbstractSearch *searchAlgorithm;

		vector<int> inputArray;
		int searchedValue;

		//
		virtual void inputLog(); // All algorithm input values
		virtual void findMaxRounds(); // max rounds to find value - find it and write logs
		virtual void findValue(); // find value and write logs
	};
}
