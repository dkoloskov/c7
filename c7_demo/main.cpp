// ISO C++ 17 Standard
#include <iostream>
#include "search/SearchAlgorithmsDemo.h"
#include "sort/SortAlgorithmsDemo.h"
#include "misc/MiscDemo.h"

using namespace c7_demo;


void searchAlgorithmsDemo();
void sortingAlgorithmsDemo();
void miscDemo();

int main(int argc, char *argv[])
{
	//searchAlgorithmsDemo();
	//sortingAlgorithmsDemo();
	miscDemo();

	return 0;
}

void searchAlgorithmsDemo()
{
	SearchAlgorithmsDemo *demo = new SearchAlgorithmsDemo();
	demo->run();
	delete demo;
}

void sortingAlgorithmsDemo()
{
	SortAlgorithmsDemo *demo = new SortAlgorithmsDemo();
	demo->run();
	delete demo;
}

void miscDemo()
{
	MiscDemo *demo = new MiscDemo();
	demo->run();
	delete demo;
}
