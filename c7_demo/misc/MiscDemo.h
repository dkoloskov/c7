#pragma once
#include "c7/data/Data.h"
#include "c7/utils/Timer.h"
#include "c7/misc/Misc.h"

using namespace c7;


// Separate namespace created to avoid situations like: "using c7" (which will get also demo classes)
namespace c7_demo
{
	/**
	 * Miscellaneous Demo demo
	 */
	class MiscDemo
	{
	public:
		MiscDemo();
		~MiscDemo();

		void run();

	private:
		Data *data;
		Timer *timer;
		Misc *misc;

		void factorialExamples();
		void fibonacciExamples();
		void palindromeExample();
		void powExample();
	};
}
