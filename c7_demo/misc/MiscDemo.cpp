#include "MiscDemo.h"
#include <string>
#include "c7/utils/Log.h"

using namespace c7;
using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
MiscDemo::MiscDemo()
{
	data = new Data();
	timer = new Timer();
	misc = new Misc();
}

void MiscDemo::run()
{
	//factorialExamples();
	fibonacciExamples();
	//palindromeExample();
	//powExample();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
// ======================================== Factorial
void MiscDemo::factorialExamples()
{
	// Iterative
	printf("3! is: %d	[iterative] \n", misc->factorial_iterative(3)); // 6
	printf("5! is: %d	[iterative] \n", misc->factorial_iterative(5)); // 120

	// Recursive
	printf("3! is: %d	[recursive] \n", misc->factorial_recursive(3)); // 6
	printf("5! is: %d	[recursive] \n\n", misc->factorial_recursive(5)); // 120
}

// ======================================== Fibonacci
void MiscDemo::fibonacciExamples()
{
	std::string s = "Fibonacci numbet at position %d is: %d";
	// Iterative
	std::string iterativeStr = s + "	[iterative] \n";
	printf(iterativeStr.c_str(), 4, misc->fibonacci_iterative(4)); // 3
	printf(iterativeStr.c_str(), 7, misc->fibonacci_iterative(7)); // 13
	printf(iterativeStr.c_str(), 15, misc->fibonacci_iterative(15)); // 610
	Log::line();
	Log::blank();
	
	// Recursive
	std::string recursiveStr = s + "	[recursive] \n";
	printf(recursiveStr.c_str(), 4, misc->fibonacci_recursive(4)); // 3
	printf(recursiveStr.c_str(), 7, misc->fibonacci_recursive(7)); // 13
	//
	misc->resetFibCounter();
	printf(recursiveStr.c_str(), 15, misc->fibonacci_recursive(15)); // 610
	printf("Recursive function calls num: %d \n", misc->getFibCounter());
	// Note: line above without cache (fibMemo) does 3996 calls! While with cache, only 32 calls!!!
	Log::line();
	Log::blank();
}

// ======================================== Palindrome
// Note: Palindrome - a word, phrase, that reads the same backward as forward: "Madam", "Rotor", "Nurses run".
void MiscDemo::palindromeExample()
{
	std::string s = "rotor";
	std::string resultStr = misc->isPalindrome_recursive(s) ? "Palindrome" : "Not Palindrome";
	printf("\"%s\" is: %s	[recursive] \n", s.c_str(), resultStr.c_str());

	s = "rocker";
	resultStr = misc->isPalindrome_recursive(s) ? "Palindrome" : "Not Palindrome";
	printf("\"%s\" is: %s	[recursive] \n\n", s.c_str(), resultStr.c_str());
}

// ======================================== Power/Exponent
void MiscDemo::powExample()
{
	printf("2^0 is: %f	[recursive] \n", misc->pow_recursive(2, 0)); // 1
	printf("-2^3 is: %f	[recursive] \n", misc->pow_recursive(-2, 3)); // -8
	printf("2^8 is: %f	[recursive] \n\n", misc->pow_recursive(2, 8)); // 256
	//
	printf("-3^4 is: %f	[recursive] \n", misc->pow_recursive(-3, 4)); // 81
	printf("5^(-2) is: %f	[recursive] \n", misc->pow_recursive(5, -2)); // 0.04
	printf("3^(-1) is: %f	[recursive] \n\n", misc->pow_recursive(3, -1)); // ~0.33...
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
MiscDemo::~MiscDemo()
{
	delete timer;
	delete data;
	delete misc;
}
