#pragma once
#include "c7/data/Data.h"
#include "c7/utils/Timer.h"
#include "c7_demo/sort/algorithms/AbstractSortDemo.h"
#include <vector>

using namespace c7;


// Separate namespace created to avoid situations like: "using c7" (which will get also demo classes)
namespace c7_demo
{
	/**
	 * Show all of Sort Algorithms demo
	 */
	class SortAlgorithmsDemo
	{
	public:
		SortAlgorithmsDemo();
		~SortAlgorithmsDemo();

		void run();

	private:
		Data *data;
		Timer *timer;

		// demo
		std::vector<AbstractSortDemo*> algorithms; // Note: I can use std::unique_ptr here, but I won't, for now
	};
}
