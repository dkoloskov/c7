#include "SortAlgorithmsDemo.h"
#include "c7_demo/sort/algorithms/BubbleSortDemo.h"
#include "c7_demo/sort/algorithms/SelectionSortDemo.h"
#include "c7_demo/sort/algorithms/InsertionSortDemo.h"

using namespace c7;
using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
SortAlgorithmsDemo::SortAlgorithmsDemo()
{
	data = new Data();
	timer = new Timer();

	// demo's
	algorithms.reserve(5);
	algorithms =
	{
		new BubbleSortDemo(data, timer),
		new SelectionSortDemo(data, timer),
		new InsertionSortDemo(data, timer)
	};
}

void SortAlgorithmsDemo::run()
{
	for (AbstractSortDemo *demo : algorithms)
	{
		demo->run();
	}
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
SortAlgorithmsDemo::~SortAlgorithmsDemo()
{
	for (AbstractSortDemo *demo : algorithms)
	{
		delete demo;
	}

	delete timer;
	delete data;
}
