#pragma once
#include "c7/data/Data.h"
#include "c7/utils/Timer.h"
#include "c7/sort/algorithms/AbstractSort.h"
#include <string>

using namespace c7;


/**
 * Base class for all Search Algorithm Demo classes
 */
namespace c7_demo
{
	class AbstractSortDemo
	{
	public:
		virtual ~AbstractSortDemo();

		virtual void run();

	protected:
		AbstractSortDemo(const Data *data, Timer *timer);

		const Data *data;
		Timer *timer;

		std::string algorithmName;
		AbstractSort *sortAlgorithm;

		vector<int> inputArray;

		//
		virtual void inputLog(); // All algorithm input values
		virtual void sort(); // sort values ascending
	};
}
