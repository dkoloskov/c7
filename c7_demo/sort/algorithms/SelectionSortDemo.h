#pragma once
#include "AbstractSortDemo.h"


namespace c7_demo
{
	class SelectionSortDemo : public AbstractSortDemo
	{
	public:
		SelectionSortDemo(const Data *data, Timer *timer);
	};
}
