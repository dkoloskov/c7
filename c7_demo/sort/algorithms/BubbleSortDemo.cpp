#include "BubbleSortDemo.h"
#include "c7/sort/algorithms/BubbleSort.h"

using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
BubbleSortDemo::BubbleSortDemo(const Data *data, Timer *timer) :
	AbstractSortDemo(data, timer)
{
	// Algorithm
	algorithmName = "Bubble Sort";
	sortAlgorithm = new BubbleSort();

	// Input data
	//inputArray = data->unsorted.numbers; // it isn't assign, it's copy of vector
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
