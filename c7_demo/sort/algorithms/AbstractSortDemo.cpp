#include "AbstractSortDemo.h"
#include "c7/utils/Log.h"
#include <string>

using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
void AbstractSortDemo::run()
{
	inputLog();
	sort();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
AbstractSortDemo::AbstractSortDemo(const Data *data, Timer *timer)
{
	this->data = data;
	this->timer = timer;

	this->inputArray = this->data->unsorted.numbers; // it isn't assign, it's copy of vector
	//this->inputArray = this->data->unsorted.thousandNumbers;
}

// log (all the usefull info)
void AbstractSortDemo::inputLog()
{
	// ======================================== LOG (all the usefull info)
	Log::title(algorithmName);
	Log::out("Input:");
	Log::out("	array: ");
	if (inputArray.size() <= 100) // Don't want many numbers in log
		Log::out(inputArray);

	Log::out("	array size: ", inputArray.size());
	Log::line();
}

void AbstractSortDemo::sort()
{
	// ======================================== Search
	timer->reset();
	inputArray = sortAlgorithm->sort(inputArray);
	double elapsedTime = timer->elapsed();

	// ======================================== LOG (all the usefull info) - part 2
	Log::line();
	Log::out("Result:");
	Log::out("	array: ");
	if (inputArray.size() <= 100) // Don't want many numbers in log
		Log::out(inputArray);
	Log::out("	search time: " + std::to_string(elapsedTime) + " seconds");

#ifdef _C7_FULL_LOG
	Log::out("	total swaps: " + std::to_string(sortAlgorithm->swapsNum));
#endif
	// Note: swapsNum counted ONLY with "_C7_FULL_LOG" flag enabled

	Log::blank();
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
AbstractSortDemo::~AbstractSortDemo()
{
	data = nullptr;
	timer = nullptr;

	//
	delete sortAlgorithm;
	sortAlgorithm = nullptr;
}
