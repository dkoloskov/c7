#include "SelectionSortDemo.h"
#include "c7/sort/algorithms/SelectionSort.h"

using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
SelectionSortDemo::SelectionSortDemo(const Data *data, Timer *timer) :
	AbstractSortDemo(data, timer)
{
	// Algorithm
	algorithmName = "Selection Sort";
	sortAlgorithm = new SelectionSort();

	// Input data
	//inputArray = data->unsorted.numbers; // it isn't assign, it's copy of vector
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
