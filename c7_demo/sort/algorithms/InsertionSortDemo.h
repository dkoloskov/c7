#pragma once
#include "AbstractSortDemo.h"


namespace c7_demo
{
	class InsertionSortDemo : public AbstractSortDemo
	{
	public:
		InsertionSortDemo(const Data *data, Timer *timer);
	};
}
