#pragma once
#include "AbstractSortDemo.h"


namespace c7_demo
{
	class BubbleSortDemo : public AbstractSortDemo
	{
	public:
		BubbleSortDemo(const Data* data, Timer* timer);
	};
}
