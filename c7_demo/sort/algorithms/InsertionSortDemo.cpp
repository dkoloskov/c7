#include "InsertionSortDemo.h"
#include "c7/sort/algorithms/InsertionSort.h"

using namespace c7_demo;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
InsertionSortDemo::InsertionSortDemo(const Data *data, Timer *timer) :
	AbstractSortDemo(data, timer)
{
	// Algorithm
	algorithmName = "Insertion Sort";
	sortAlgorithm = new InsertionSort();
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
