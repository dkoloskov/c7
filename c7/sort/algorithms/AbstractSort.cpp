#include "AbstractSort.h"
#include "c7/utils/Log.h"

using namespace c7;
//#define _C7_FULL_LOG


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
AbstractSort::AbstractSort()
{
	//log("AbstractSort constructor");
	swapsNum = 0;
}

void AbstractSort::swap(std::vector<int> &array, int index_1, int index_2)
{
	int temp = array[index_1];
	array[index_1] = array[index_2];
	array[index_2] = temp;

#ifdef _C7_FULL_LOG
	swapsNum++;
#endif
}

void AbstractSort::fullLog(const std::vector<int>& array, int currentIndex)
{
	if (array.size() <= 20)
	{
		string s = Utils::arrayToString(array) + "	index: " + to_string(currentIndex);
		Log::out(s);
	}
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
AbstractSort::~AbstractSort()
{
	//log("AbstractSort destructor");
}
