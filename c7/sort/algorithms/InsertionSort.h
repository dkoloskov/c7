#pragma once
#include <vector>
#include "AbstractSort.h"


namespace c7
{
	class InsertionSort : public AbstractSort
	{
	public:
		virtual std::vector<int> &sort(std::vector<int> &array) override;
	};
}
