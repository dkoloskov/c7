#pragma once
#include "AbstractSort.h"


namespace c7
{
	class BubbleSort : public AbstractSort
	{
	public:
		virtual std::vector<int> &sort(std::vector<int> &array) override;

	protected:
		void fullLog(const std::vector<int> &array, int i, int j);
	};
}
