#include "BubbleSort.h"
#include "c7/utils/Log.h"
#include "c7/utils/Utils.h"
#include <string>

using c7::BubbleSort, std::to_string;
//#define _C7_FULL_LOG


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
std::vector<int> &BubbleSort::sort(std::vector<int> &array)
{
	if (array.empty())
		return array;

	for (int i = 0; i < array.size() - 1; i++)
	{
		for (int j = 0; j < array.size() - (1 + i); j++)
		{
			if (array[j + 1] < array[j])
			{
				swap(array, j, j + 1);

#ifdef _C7_FULL_LOG
				fullLog(array, i, j);
#endif
			}
		}

#ifdef _C7_FULL_LOG
		Log::blank();
#endif
	}

	return array;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
void BubbleSort::fullLog(const std::vector<int> &array, int i, int j)
{
	if (array.size() <= 20)
	{
		string s = Utils::arrayToString(array) + "	i:" + to_string(i) + " j:" + to_string(j);
		Log::out(s);
	}
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
