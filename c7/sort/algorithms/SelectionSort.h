#pragma once
#include <vector>
#include "AbstractSort.h"


namespace c7
{
	class SelectionSort : public AbstractSort
	{
	public:
		virtual std::vector<int> &sort(std::vector<int> &array) override;

	protected:
		int indexOfMinimum(std::vector<int> &array, int startIndex);
	};
}
