#include "InsertionSort.h"
#include "c7/utils/Log.h"
#include "c7/utils/Utils.h"
#include <string>

using c7::InsertionSort, std::to_string;
//#define _C7_FULL_LOG


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
std::vector<int> &InsertionSort::sort(std::vector<int> &array)
{
	if (array.empty())
		return array;

	//
	int currentValue;
	for (int i = 1; i < array.size(); i++)
	{
		currentValue = array[i];

		int j;
		for (j = i; j > 0; j--)
		{
			if (array[j - 1] > currentValue)
				array[j] = array[j - 1]; // shift all the "bigger" values "to the right" (�������)
			else
				break;
		}
		array[j] = currentValue; // move currentValue to correct position

#ifdef _C7_FULL_LOG
		fullLog(array, i);
#endif
	}

	return array;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
