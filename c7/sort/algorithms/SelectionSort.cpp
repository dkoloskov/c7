#include "SelectionSort.h"
#include "c7/utils/Log.h"
#include "c7/utils/Utils.h"
#include <string>

using c7::SelectionSort, std::to_string;
//#define _C7_FULL_LOG


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
std::vector<int> &SelectionSort::sort(std::vector<int> &array)
{
	if (array.empty())
		return array;

	//
	int minValueIndex;

	for (int i = 0; i < array.size() - 1; i++)
	{
		minValueIndex = indexOfMinimum(array, i);
		swap(array, i, minValueIndex);

#ifdef _C7_FULL_LOG
		fullLog(array, i);
#endif
	}

	return array;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------
int SelectionSort::indexOfMinimum(std::vector<int> &array, int startIndex)
{
	int minIndex = startIndex;
	int minValue = array[startIndex];

	for (int i = minIndex + 1; i < array.size(); i++)
	{
		if (array[i] < minValue)
		{
			minIndex = i;
			minValue = array[i];
		}
	}

	return minIndex;
}

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
