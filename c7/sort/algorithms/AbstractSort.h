#pragma once
#include <vector>


namespace c7
{
	/**
	 * Base class for all Sort Algorithm classes
	 */
	class AbstractSort
	{
	public:
		virtual ~AbstractSort();

		int swapsNum; // number of swaps for comparison between sorting algorithms

		virtual std::vector<int> &sort(std::vector<int> &array) = 0;

	protected:
		AbstractSort();

		virtual void swap(std::vector<int> &array, int index_1, int index_2);

		// Debug stuff
		virtual void fullLog(const std::vector<int>& array, int currentIndex);
	};
}
