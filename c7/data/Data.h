#pragma once
#include <vector>

using std::vector;


namespace c7
{
	class Data
	{
	private:
		// +++++++++++++++++++++++++ SORTED DATA +++++++++++++++++++++++++
		class SortedData
		{
		public:
			SortedData()
			{
				fillUpData(hundredNumbers, 100);
				fillUpData(thousandNumbers, 1000);
				fillUpData(millionNumbers, 1000000);
			}

			// ======================================== 100 / 1000 / 1000 000
			vector<int> hundredNumbers;
			vector<int> thousandNumbers;
			vector<int> millionNumbers;

			// ======================================== Prime Numbers (������� �����)
			const vector<int> primeNumbers =
				{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
		};
		// ------------------------- SORTED DATA -------------------------

		// +++++++++++++++++++++++++ UNSORTED DATA +++++++++++++++++++++++++
		class UnsortedData
		{
		public:
			UnsortedData()
			{
				fillUpData(hundredNumbers, 100, false);
				fillUpData(thousandNumbers, 1000, false);
			}

			// ======================================== Random unsorted numbers
			const vector<int> numbers = {99, -1, 7, 3, 0, 22, 44, -5, 11};
			const vector<int> numbers_sorted_result = {-5, -1, 0, 3, 7, 11, 22, 44, 99};

			// ======================================== 100 / 1000 / 1000 000
			vector<int> hundredNumbers;
			vector<int> thousandNumbers;
		};
		// ------------------------- UNSORTED DATA -------------------------

		// ==================================================
		static void fillUpData(vector<int> &array, int amount, bool ascending = true)
		{
			int startIndex = ascending ? 0 : amount;
			int endIndex = ascending ? amount : 0;
			int operation = ascending ? 1 : -1; // increment OR decrement

			for (int i = startIndex; i != endIndex; i += operation)
			{
				array.push_back(i);
			}
		}

	public:
		// These lines here, because compiler need Sorted/UnsortedData to be declared before memory allocation here
		SortedData sorted;
		UnsortedData unsorted;
	};
}
