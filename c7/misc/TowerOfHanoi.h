#pragma once
#include <vector>


/**
 * Tower of Hanoi solving class
 */
class TowerOfHanoi
{
public:
	TowerOfHanoi(int disksNum);

	std::vector<int> pegA;
	std::vector<int> pegB;
	std::vector<int> pegC;
	// Note: peg - �������, ������
	
	void solveHanoi_recursive(int diskNum, std::vector<int> &fromPeg, std::vector<int> &toPeg);

private:
	std::vector<int>& getSparePeg();
	void moveDisk(std::vector<int>& fromPeg, std::vector<int>& toPeg);
};
