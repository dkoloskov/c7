#pragma once
#include <string>
#include <unordered_map>


namespace c7
{
	/**
	 * Miscellaneous
	 */
	class Misc
	{
	public:
		// ======================================== Factorial
		int factorial_iterative(int n);
		int factorial_recursive(int n);

		// ======================================== Fibonacci
		int fibonacci_iterative(int numInSequence); // iterative - �����������, �������������

		// Recursive
		std::unordered_map<int, int> fibMemo; // cache for recursive algorithm optimization

		int fibonacci_recursive(int numInSequence);
		
		inline void resetFibCounter() { fibCounter = 0; }
		inline int getFibCounter() { return fibCounter; }
		// Note: Need to move Fibonacci to separate class, when Misc will grow

		// ======================================== Palindrome
		// Note: Palindrome - a word, phrase, that reads the same backward as forward: "Madam", "Rotor", "Nurses run".
		bool isPalindrome_recursive(std::string s);

		// ======================================== Power/Exponent
		float pow_recursive(int num, int exp);

	private:
		// ======================================== Fibonacci
		int fibCounter = 0;
	};
}
