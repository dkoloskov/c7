#include "TowerOfHanoi.h"
#include <stdexcept>


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
TowerOfHanoi::TowerOfHanoi(int disksNum)
{
	if (disksNum == 0)
		throw std::logic_error("disksNum == 0");

	// Setup pegs capacity
	pegA.reserve(disksNum);
	pegB.reserve(disksNum);
	pegC.reserve(disksNum);

	// Setup all disks in pegA
	for(int i=1; i<= disksNum; i++)
	{
		pegA[i - 1] = i;
	}
}

void TowerOfHanoi::solveHanoi_recursive(int diskNum, std::vector<int>& fromPeg, std::vector<int>& toPeg)
{
	
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------

