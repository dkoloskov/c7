#include "Misc.h"
#include <iostream>

using std::cout, std::endl;
using namespace c7;


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
// ======================================== Factorial
int Misc::factorial_iterative(int n)
{
	if (n < 0)
		return -1; // because factorial if for 0 and above numbers

	int result = 1; // because 0! = 1
	for (int i = 1; i <= n; i++)
	{
		result *= i;
	}

	return result;
}

int Misc::factorial_recursive(int n)
{
	if (n < 0)
		return -1; // because factorial if for 0 and above numbers
	if (n == 0)
		return 1; // because 0! = 1

	return n * factorial_recursive(n - 1);
}

// ======================================== Fibonacci
int Misc::fibonacci_recursive(int numInSequence)
{
	fibCounter++; // This line only for calls measurement and can be removed
	
	if (numInSequence == 0 || numInSequence == 1)
		return numInSequence; // ������� ������ (������� ����������)
	if (fibMemo[numInSequence]) // optimization of algorithm by using cache
		return fibMemo[numInSequence];

	int result = fibonacci_recursive(numInSequence - 1) + fibonacci_recursive(numInSequence - 2);
	fibMemo[numInSequence] = result;
	
	return result;
}

// Note: while fibonacci_recursive() goes Top -> Bottom, fibonacci_iterative() goes Bottom -> Top
int Misc::fibonacci_iterative(int numInSequence)
{
	if (numInSequence == 0 || numInSequence == 1)
		return numInSequence; // ������� ������ (������� ����������)
	
	int twoBehind = 0; // fib(value-2)
	int oneBehind = 1; // fib(value-1)
	int result = 0;

	for (int i = 0; i < numInSequence-1; i++)
	{
		result = oneBehind + twoBehind;

		//
		twoBehind = oneBehind;
		oneBehind = result;
	}

	return result;
}

// ======================================== Palindrome
// Note: Palindrome - a word, phrase, that reads the same backward as forward: "Madam", "Rotor", "Nurses run".
bool Misc::isPalindrome_recursive(std::string s)
{
	if (s.length() == 0 || s.length() == 1)
		return true;

	if (s.at(0) == s.at(s.length() - 1))
	{
		int startStrIndex = 1;
		int symbolsToCopy = s.length() - 2;
		std::string subS = s.substr(startStrIndex, symbolsToCopy);

		//cout << subS << endl;
		if (isPalindrome_recursive(subS))
			return true;
	}

	return false;
}

// ======================================== Power/Exponent
/**
 * Complexity: O(n) for exp < 3
 *			   O( log_2(2n/3) ) for exp > 3
 *			   Note: this is NOT 100% precise complexity formula, but it's close enough for me right now
 */
float Misc::pow_recursive(int num, int exp)
{
	if (exp == 0)
		return 1.f;
	else if (exp < 0)
	{
		return 1.f / pow_recursive(num, exp * (-1));
	}
	else if (exp % 2 == 0) // if even (������)
	{
		// With this approach, complexity become O( log_2(2n/3)  )
		float powDoneNum = pow_recursive(num, exp / 2);
		return powDoneNum * powDoneNum;

		// >>> IF EVEN Explanation <<<
		// num^(n/2) * num^(n/2) = num^(n/2 + n/2) = num^(2n/2) = num^n
	}

	// Only this aproach will be O(n) complexity
	return num * pow_recursive(num, --exp);
}

// ======================== pow_recursive complexity explanation ========================
// Geometric Sequence Formula: ar^(n-1)
//		a - first term
//		r - factor between the terms
//		
// For pow_recursive() I found sequence since number 3: 3, 6, 12, 24, 48
//		Example 1, exp=50 sequence: 50, 25, 24, 12, 6, 3, 2, 1, 0
//		Example 2, exp=49 sequence: 49, 48, 24, 12, 6, 3, 2, 1, 0
//			for some numbers picture will be different, but, we don't care about diff of (+3)-(+5) in Big-O notation
//		
//		Note: "... 2, 1, 0" - exp for this numbers always constant, so "O(n) for exp < 3"
//
// So, with "Geometric Sequence Formula" I found formula for "i_th number of sequence"
//		3 * 2^(i-1)
//		i - number of term (����� ����� ������������������)
//
// Then I solved equation of "pow_recursive() complexity for exp=n"
//		3 * 2^(i-1) = n
//		2^(i-1) = n/3
//		2^i / 2 = n/3
//		2^i = 2n/3
//		log_2(2^i) = log_2(2n/3)    - take log_2() from both sides of equation. Note: log_2() is my notation for lg()
//			i * log_2(2) = i * 1 = i
//		i = log_2(2n/3) <----- SOLVED. This is complexity formula for pow_recursive()

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
