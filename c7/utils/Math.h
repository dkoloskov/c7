#pragma once


/**
 * My utility class for work with math.
 * 
 * Note: I know that some libs there id web already has this functionality. But I don't need all that,
 * it's faster for me to implement few methods that I need.
 */
namespace c7
{
	class Math
	{
	public:
		static double log(int base, int result);
		static double log(double base, double result);
	};
}
