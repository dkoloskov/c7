#pragma once
#include <string>
#include <vector>


namespace c7
{
	/**
	 * Class for random utils.
	 * Note: In future functionality can be moved/renamed.
	 */
	class Utils
	{
	public:
		template <typename T>
		static std::string arrayToString(const std::vector<T> array, std::string separator = " ");
	};


	// ======================================== Templates
	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	template <typename T>
	std::string Utils::arrayToString(const std::vector<T> array, std::string separator)
	{
		std::string s = "";
		for (T n : array)
		{
			s += std::to_string(n) + separator;
		}

		return s;
	}

	//--------------------------------------------------------------------------
	//							PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							PRIVATE METHODS
	//--------------------------------------------------------------------------
}
