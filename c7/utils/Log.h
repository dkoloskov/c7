#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "Utils.h"

using std::string, std::vector, std::cout, std::endl, std::to_string;


namespace c7
{
	/**
	 * Logging class
	 * 
	 * Note: instead of template class, I made only few template methods,
	 * because when I try to call "template class::methodWithoutT()" i get error: "argument list for class template ... is missing"
	 */
	class Log
	{
	public:
		// ======================================== Main log functions
		static void out(const string text);

		template <typename T>
		static void out(const string text, const T value);

		template <typename T>
		static void out(const vector<T> array);

		// ======================================== Syntactic sugar
		// I know that this is a bad idea to create such wrappers,
		// but if I will need to write logs to file, that will be useful
		static void blank();
		static void line();

		/**
		 * Print "title of section"
		 */
		static void title(const string text); // Not sure, maybe it's here temporarily
	};


	// ======================== TEMPLATES ========================
	//--------------------------------------------------------------------------
	//							PUBLIC METHODS
	//--------------------------------------------------------------------------
	// ======================================== Main log functions
	template <typename T>
	void Log::out(const string text, const T value) // static
	{
		cout << text << value << endl;
	}

	template <typename T>
	void Log::out(const vector<T> array) // static
	{
		string s = Utils::arrayToString(array);
		cout << s << endl;
	}

	//--------------------------------------------------------------------------
	//							PROTECTED METHODS
	//--------------------------------------------------------------------------

	//--------------------------------------------------------------------------
	//							PRIVATE METHODS
	//--------------------------------------------------------------------------
}
