#pragma once

#include <chrono> // ��� ������� �� std::chrono
// Many useful info about "chrono" here: https://habr.com/ru/post/324984/


/**
 * c7 - short from [c7]ever
 */
namespace c7
{
	// alias (���������� �����) ��������� ��� �������� ������� � ��������� �����
	using clockHR_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1>>;

	// std::chrono::high_resolution_clock - ���� � ���������� ������������������ ����, ��������� � �������.
	// 
	// std::chrono::duration<Rep, std::ratio<Period>> - ��������� ��������.
	// Rep - ���, �������������� ���������� �����
	// Period - ��������� std::ratio, �������������� ������ ���� (�. �. ���������� ������ �� ���)

	// std::ratio<num, den> - rational number type, "numerator" divide by "denominator"
	// Useful when conversion of measurement units (������ ���������) required

	/**
	 * Class copied from example (with my changes and comments)
	 * https://ravesli.com/urok-129-tajming-koda-vremya-vypolneniya-programmy/
	 */
	class Timer
	{
	public:
		Timer();

		void reset();
		double elapsed() const;

	private:
		std::chrono::time_point<clockHR_t> startPoint;
		// std::chrono::time_point<high_resolution_clock> - ������ �������.
	};
}
