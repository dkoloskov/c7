#include "SearchResult.h"


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
c7::SearchResult::SearchResult()
{
	reset();
}

void c7::SearchResult::reset()
{
	rounds = 0;
	foundIndex = -1;
}
