#pragma once


namespace c7
{
	/**
	 * I need this class for testing purpose.
	 * I don't think that having this class, instead of direct reture value will significantly effect algorithms
	 * time of execution.
	 */
	class SearchResult
	{
	public:
		SearchResult();

		void reset();

		int rounds; // round == iteration
		int foundIndex; // == -1 when search fail OR wrong array input
	};
}
