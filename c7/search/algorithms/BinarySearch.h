#pragma once
#include "AbstractSearch.h"
#include <vector>


namespace c7
{
	class BinarySearch : public AbstractSearch
	{
	public:
		BinarySearch();
		virtual ~BinarySearch();

		/**
		 * Binary Search algorithm
		 * Complexity: O( lg(n) )    in other notation: O( log_2(n) )
		*/
		virtual SearchResult &find(const std::vector<int> &array, int searchedValue) override;

		virtual int findMaxRounds(int arraySize) override;

	private:
		// Debug purpose only
		void fullLog(double min, double max, double guessIndex, int guessValue);
	};
}
