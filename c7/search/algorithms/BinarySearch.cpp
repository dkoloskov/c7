#include "BinarySearch.h"
#include "c7/utils/Log.h"
#include "c7/utils/Math.h"
#include <cmath>

using c7::BinarySearch, c7::SearchResult, c7::Math;
//#define _C7_FULL_LOG


//--------------------------------------------------------------------------
//							PUBLIC METHODS
//--------------------------------------------------------------------------
BinarySearch::BinarySearch()
{
	//Log::out("BinarySearch constructor");
}

SearchResult &BinarySearch::find(const vector<int> &array, int searchedValue)
{
	result->reset();
	if (array.size() == 0)
		return *result;


	// Step 1
	double min = 0;
	double max = array.size() - 1;
	//
	double guessIndex;
	int guessValue;

	while (true)
	{
		result->rounds++;

		// Step 2 (guess the average of max and min, rounded down to be integer)
		guessIndex = std::floor((min + max) / 2);
		guessValue = array.at(guessIndex);

#ifdef _C7_FULL_LOG
		fullLog(min, max, guessIndex, guessValue);
#endif

		if (guessValue == searchedValue) // Step 3
			break;
		else if (guessValue < searchedValue) // Step 4
			min = ++guessIndex;
		else if (guessValue > searchedValue) // Step 5
			max = --guessIndex;

		if (min > max) // Step 6
		{
			guessIndex = -1;
			break;
		}
	}

	result->foundIndex = guessIndex;
	return *result;
}

int BinarySearch::findMaxRounds(int arraySize)
{
	if (arraySize <= 0)
		return 0;

	double value = Math::log(2, arraySize);
	return std::floor(value) + 1;
}

//--------------------------------------------------------------------------
//							PROTECTED METHODS
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//							PRIVATE METHODS
//--------------------------------------------------------------------------
void BinarySearch::fullLog(double min, double max, double guessIndex, int guessValue)
{
	Log::out("Round ", result->rounds);
	Log::out<int>("min ", min);
	Log::out<int>("max ", max);
	Log::out<int>("guessIndex ", guessIndex);
	Log::out<int>("guessValue ", guessValue);
	Log::blank();
}

//--------------------------------------------------------------------------
//								DESTROY
//--------------------------------------------------------------------------
BinarySearch::~BinarySearch()
{
	//Log::out("BinarySearch destructor");
}
