#pragma once
#include "c7/search/SearchResult.h"
#include <vector>


namespace c7
{
	/**
	 * Base class for all Search Algorithm classes
	 */
	class AbstractSearch
	{
	public:
		virtual ~AbstractSearch();
		
		/**
		* \param array - input vector
		* \param searchedValue - searched value
		* \return - SearchResult
		*/
		virtual SearchResult &find(const std::vector<int> &array, int searchedValue) = 0;
		// TODO 2: make duplicate with std::array<int>, and compare speed
		// TODO 3: make duplicate with C array and also compare speed (I hope this will be slower that std::array...)

		/**
		 * \return - max rounds that is required to find value (can be less).
		 */
		virtual int findMaxRounds(int arraySize) = 0;

	protected:
		AbstractSearch();

		SearchResult *result;
	};
}
