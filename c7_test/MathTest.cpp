#include "c7/utils/Math.h"


namespace c7_test
{
	/*class MathTest : public ::testing::Test
	{
	protected:
		// Code here will be called immediately after the constructor (right before each test).
		void SetUp() override
		{
		}

		// Code here will be called immediately after each test (right before the destructor).
		void TearDown() override
		{
		}
	}*/

	TEST(MathTestCase, Log)
	{
		// 2
		ASSERT_EQ(c7::Math::log(2, 4), 2);
		ASSERT_EQ(c7::Math::log(2, 1024), 10);
		ASSERT_EQ(c7::Math::log(2, 1), 0);
		ASSERT_EQ(c7::Math::log(2.0f, 0.5f), -1);

		// 3
		ASSERT_EQ(c7::Math::log(3, 27), 3);
	}

	// TODO: create "Log_erroneous" tests for values: 0, -1
	// Note: For now it's not critical, but in future Error must be thrown AND unit test must pass by getting an error for such values.
}
