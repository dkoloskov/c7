//#include "pch.h"
// Note: line above isn't required, because I used "force", to include it to all files in project.
#include "c7/sort/algorithms/AbstractSort.h"
#include "c7/sort/algorithms/BubbleSort.h"
#include "c7/sort/algorithms/SelectionSort.h"
#include "c7/sort/algorithms/InsertionSort.h"
#include "c7/data/Data.h"
#include <iostream>

using namespace c7;


/**
 * Typed Tests
 * https://github.com/google/googletest/blob/master/googletest/docs/advanced.md#typed-tests
 */
namespace c7_test
{
	/**
	 * The "fixture" (приспособление).
	 * Fixture allows to reuse the same configuration of objects for several different tests.
	 */
	template <class T>
	class SortTest : public ::testing::Test
	{
	public:
		// Per-test-suite set-up.
		// Called before the first test in this test suite. Can be omitted if not needed.
		// PURPOSE: to have shared resource (created only ONCE) for few tests
		static void SetUpTestCase()
		{
			// Note: in documentation method called: SetUpTestSuite()
			// Renamed a while ago, apparently VS 2019 still use old gtest lib version.

			data = new Data();
		}

		// Per-test-suite tear-down.
		// Called after the last test in this test suite. Can be omitted if not needed.
		static void TearDownTestCase()
		{
			// Note: in documentation method called: TearDownTestSuite()

			delete data;
			data = nullptr;
		}

	protected:
		static Data *data;
		T sortAlgorithm;

		// Code here will be called immediately after the constructor (right before each test).
		void SetUp() override
		{
			
		}

		// Code here will be called immediately after each test (right before the destructor).
		void TearDown() override
		{
			
		}
	};

	// Initialize static variable
	template <typename T>
	Data *SortTest<T>::data = nullptr;

	// ======================================== Initialize types to test
	using MyTypes = ::testing::Types<BubbleSort, SelectionSort, InsertionSort>; // <-----
	//TYPED_TEST_SUITE(SortTest, MyTypes);
	TYPED_TEST_CASE(SortTest, MyTypes);

	// ======================================== Tests
	TYPED_TEST(SortTest, SortNumbers)
	{
		// cast, so it will be easier to work with variable
		AbstractSort *sortAlgorithm = &this->sortAlgorithm;

		std::vector<int> array = this->data->unsorted.numbers; // copy array
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, this->data->unsorted.numbers_sorted_result);
	}

	TYPED_TEST(SortTest, SortNumbers_WrongInputArray)
	{
		// cast, so it will be easier to work with variable
		AbstractSort* sortAlgorithm = &this->sortAlgorithm;
		
		std::vector<int> array, sortedArray = {};
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, sortedArray);
	}

	TYPED_TEST(SortTest, SortNumbers_WrongInputArray_2)
	{
		// cast, so it will be easier to work with variable
		AbstractSort* sortAlgorithm = &this->sortAlgorithm;
		
		std::vector<int> array = { 1 }, sortedArray = { 1 };
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, sortedArray);
	}
	
	// ======================================== Template below
	/*TYPED_TEST(SortTest, Template)
	{
		// Inside a test, refer to the special name TypeParam to get the type
		// parameter.  Since we are inside a derived class template, C++ requires
		// us to visit the members of FooTest via 'this'.
		TypeParam n = this->value_;

		// To visit static members of the fixture, add the 'TestFixture::'
		// prefix.
		n += TestFixture::shared_;

		// To refer to typedefs in the fixture, add the 'typename TestFixture::'
		// prefix.  The 'typename' is required to satisfy the compiler.
		typename TestFixture::List values;
	}*/
}
