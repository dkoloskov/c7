//#include "pch.h"
// Note: line above isn't required, because I used "force", to include it to all files in project.
#include "c7/data/Data.h"
#include "c7/search/algorithms/BinarySearch.h"


namespace c7_test
{
	/**
	 * The "fixture" (приспособление) for testing class: "BinarySearch".
	 * Fixture allows to reuse the same configuration of objects for several different tests.
	 */
	class BinarySearchTest : public ::testing::Test
	{
	protected:
		c7::Data *data;
		c7::BinarySearch *searchAlgorithm;

		// Code here will be called immediately after the constructor (right before each test).
		void SetUp() override
		{
			data = new c7::Data();
			searchAlgorithm = new c7::BinarySearch();
		}

		// Code here will be called immediately after each test (right before the destructor).
		void TearDown() override
		{
			delete data;
			delete searchAlgorithm;
		}
	};

	/*TEST(TestCaseName, TestName)
	{
		EXPECT_EQ(1, 1);
		EXPECT_TRUE(true);
		ASSERT_EQ(1, 0);
	}*/

	// TEST_F - for tests with "Fixtures" (with )
	TEST_F(BinarySearchTest, BinarySearch_FindNumber)
	{
		const vector<int> numbers_sorted = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		c7::SearchResult result = searchAlgorithm->find(numbers_sorted, 1);
		ASSERT_EQ(result.foundIndex, 0);

		result = searchAlgorithm->find(numbers_sorted, 9);
		ASSERT_EQ(result.foundIndex, 8);

		result = searchAlgorithm->find(numbers_sorted, 3);
		ASSERT_EQ(result.foundIndex, 2);

		result = searchAlgorithm->find(numbers_sorted, 5);
		ASSERT_EQ(result.foundIndex, 4);
	}

	TEST_F(BinarySearchTest, BinarySearch_MissingNumber)
	{
		const vector<int> numbers_sorted = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		c7::SearchResult result = searchAlgorithm->find(numbers_sorted, 11);
		ASSERT_EQ(result.foundIndex, -1);

		result = searchAlgorithm->find(numbers_sorted, -2);
		ASSERT_EQ(result.foundIndex, -1);
	}

	TEST_F(BinarySearchTest, BinarySearch_WrongInputArray)
	{
		c7::SearchResult result = searchAlgorithm->find({}, 11);
		ASSERT_EQ(result.foundIndex, -1);

		result = searchAlgorithm->find({1}, 1);
		ASSERT_EQ(result.foundIndex, 0);
	}

	//Formula: Max Rounds = Math.floor( log_2(numbers.size()) ) + 1
	TEST_F(BinarySearchTest, BinarySearch_MaxRounds)
	{
		// Find num: 4. This will take max rounds for current amount of numbers
		vector<int> numbers = {1, 2, 3, 4};
		c7::SearchResult result = searchAlgorithm->find(numbers, 4);
		int maxRounds = searchAlgorithm->findMaxRounds(numbers.size());
		ASSERT_EQ(result.rounds, maxRounds);
		ASSERT_EQ(result.rounds, 3); // (2^2) + 1

		// Find num: 7 ...
		numbers = {1, 2, 3, 4, 5, 7};
		result = searchAlgorithm->find(numbers, 7);
		maxRounds = searchAlgorithm->findMaxRounds(numbers.size());
		ASSERT_EQ(result.rounds, maxRounds);
		ASSERT_EQ(result.rounds, 3); // (2^Math.ceil(2.xxx)) + 1

		// Find num: 1024 ...
		numbers = {};
		for (int i = 0; i < 1024; i++)
			numbers.push_back(i);
		result = searchAlgorithm->find(numbers, 1024);
		maxRounds = searchAlgorithm->findMaxRounds(numbers.size());
		ASSERT_EQ(result.rounds, maxRounds);
		ASSERT_EQ(result.rounds, 11); // (2^10) + 1
	}

	
	// TODO:: ADD HERE CHECKS OF rounds approximation!
}
