//#include "pch.h"
// Note: line above isn't required, because I used "force", to include it to all files in project.
#include "c7/sort/algorithms/SelectionSort.h"
#include "c7/data/Data.h"
#include <iostream>

/**
 * This file contains example of tests for one specific class - SelectionSort.
 * And it's functionality is "DISABLED_"!!! <---------------------------------------------
 * 
 * Functionality of this class is implemented for children of AbstractSort in SortTest.cpp.
 * So, this class exists only for demonstration purpose.
 *
 * TODO: remove this file, and move "SelectionSort" tests to SortTest.cpp, when will have other file fith tests and fixture ( TEST_F(...) )
 */
namespace c7_test
{
	/**
	 * The "fixture" (приспособление).
	 * Fixture allows to reuse the same configuration of objects for several different tests.
	 */
	class SelectionSortTest : public ::testing::Test
	{
	public:
		// Per-test-suite set-up.
		// Called before the first test in this test suite. Can be omitted if not needed.
		// PURPOSE: to have shared resource (created only ONCE) for few tests
		static void SetUpTestCase()
		{
			// Note: in documentation method called: SetUpTestSuite()
			// Renamed a while ago, apparently VS 2019 still use old gtest lib version.

			data = new c7::Data();
		}

		// Per-test-suite tear-down.
		// Called after the last test in this test suite. Can be omitted if not needed.
		static void TearDownTestCase()
		{
			// Note: in documentation method called: TearDownTestSuite()

			delete data;
			data = nullptr;
		}

	protected:
		static c7::Data *data;
		c7::SelectionSort *sortAlgorithm;

		// Code here will be called immediately after the constructor (right before each test).
		void SetUp() override
		{
			sortAlgorithm = new c7::SelectionSort();
		}

		// Code here will be called immediately after each test (right before the destructor).
		void TearDown() override
		{
			delete sortAlgorithm;
		}
	};

	c7::Data *SelectionSortTest::data = nullptr;

	/*TEST(TestCaseName, TestName)
	{
		EXPECT_EQ(1, 1);
		EXPECT_TRUE(true);
		ASSERT_EQ(1, 0);
	}*/

	// TEST_F - for tests with "Fixtures" (with )
	TEST_F(SelectionSortTest, DISABLED_SortNumbers)
	{
		std::vector<int> array = data->unsorted.numbers; // copy array
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, data->unsorted.numbers_sorted_result);
	}

	TEST_F(SelectionSortTest, DISABLED_SelectionSort_WrongInputArray)
	{
		std::vector<int> array, sortedArray = {};
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, sortedArray);
	}

	TEST_F(SelectionSortTest, DISABLED_SelectionSort_WrongInputArray_2)
	{
		std::vector<int> array = {1}, sortedArray = {1};
		array = sortAlgorithm->sort(array);
		ASSERT_EQ(array, sortedArray);
	}
}
